#!/bin/bash

sudo rm -r ~/builds
mkdir -p ~/builds/x86_64 ~/builds/build
cd ~/builds/build
sudo pacman -Syy -y
#############################################
## Add all AUR package names to line below ##
#############################################
for i in  aic94xx-firmware ast-firmware geforcenow-electron prismlauncher-git radiotray-ng upd72020x-fw wd719x-firmware wlrobs

do
	git clone "https://aur.archlinux.org/""$i"".git"
	cd "$i"
	makepkg -s
	mv "$i"*".tar.zst" ../../x86_64/
	cd ~/builds/x86_64
	repo-add builds.db.tar.gz "$i"*".tar.zst"
        cd ~/builds/build
done
