#!/bin/bash
sudo pacman -S pacutils perl-libwww perl-term-ui perl-json perl-data-dump perl-lwp-protocol-https perl-term-readline-gnu -y
git clone https://aur.archlinux.org/trizen.git 
cd trizen
makepkg -i
cd ..
sudo rm -R trizen
trizen -S $(cat packagesx86_64 | cut -d' ' -f1)
sudo cp airootfs/usr/share/wayland-sessions/sway.desktop /usr/share/wayland-sessions
sed -i 's/live/$USER/g' airootfs/etc/sddm.conf.d/autologin.conf
sudo cp -R airootfs/etc/sddm.conf.d /etc
cp -R airootfs/etc/skel/.config ~/   
sudo groupadd -r autologin
sudo gpasswd -a $USER autologin
sudo systemctl enable NetworkManager.service
sudo systemctl enable bluetooth.service
sudo systemctl enable sddm.service
sudo systemctl enable libvirtd.service
sudo systemctl start  libvirtd.service
sudo sed -i 's/#unix_sock_group = "libvirt"/unix_sock_group = "libvirt"/g' /etc/libvirt/libvirtd.conf
sudo sed -i 's/#unix_sock_ro_perms = "0777"/unix_sock_ro_perms = "0770"/g' /etc/libvirt/libvirtd.conf
sudo virsh net-autostart default
sudo virsh net-start default