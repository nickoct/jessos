# Set Keyboard Layout
input *  xkb_layout "gb" 

# MultiMonitor
output Virtual-1 pos 0 0 res 1680x1050
output HDMI-A-1 pos 0 0 res 1920x1080
output DP-3 pos 0 1080 res 1680x1050

# Adding a Background

exec swaybg -o DP-3 -i ~/.local/share/backgrounds/WoodlandStream.jpg 
exec swaybg -o HDMI-A-1 -i ~/.local/share/backgrounds/BlueHorrizon.jpg

# Wayland environment variables
exec systemctl --user import-environment WAYLAND_DISPLAY XDG_CURRENT_DESKTOP
exec dbus-update-activation-environment --systemd WAYLAND_DISPLAY XDG_CURRENT_DESKTOP=sway

set $mod Mod4
#set $mod Mod1

exec --no-startup-id veikk-startup.sh
#font pango:Terminus 30
font pango:Cantarell 30px
#exec_always --no-startup-id bash ~/.screenlayout/mylayout.sh
bindsym $mod+p exec --no-startup-id /opt/piavpn/bin/pia-client 
exec --no-startup-id volumeicon
exec_always --no-startup-id mpd
exec --no-startup-id radiotray-ng
exec_always --no-startup-id /usr/lib/policykit-1-gnome/polkit-gnome-authentication-agent-1

# set $term xfce4-terminal
set $term foot

# Drop Down Terminal 
bindsym F12 exec --no-startup-id xfce4-terminal --drop-down
# Make all urxvts use a 1-pixel border:
for_window [class="urxvt"] border pixel 1

# Add your countries locale for correct keyboard layout
#exec_always --no-startup-id setxkbmap -layout gb
#exec --no-startup-id input * xkb_layout gb 

exec_always --no-startup-id nm-applet

#Turn off screen blanking
exec_always --no-startup-id xset s off -dpms

# Pulse Audio controls
bindsym XF86AudioRaiseVolume exec --no-startup-id pactl -- set-sink-volume @DEFAULT_SINK@ +5% #increase sound volume
bindsym XF86AudioLowerVolume exec --no-startup-id pactl -- set-sink-volume @DEFAULT_SINK@ -5% #decrease sound volume
bindsym XF86AudioMute exec --no-startup-id pactl -- set-sink-mute @DEFAULT_SINK@ toggle # mute sound


# Media player controls
bindsym XF86AudioPlay exec --no-startup-id playerctl play-pause
bindsym XF86AudioPause exec --no-startup-id playerctl play-pause
bindsym XF86AudioNext exec --no-startup-id playerctl next
bindsym XF86AudioPrev exec --no-startup-id playerctl previous

## Start DropBox
#exec_always --no-startup-id dropbox start

###---Basic Definitions---###
#Needed for i3-gaps

hide_edge_borders both
for_window [class="^.*"] border pixel 1
gaps inner 5
gaps outer 5

# Compositor
exec --no-startup-id picom



# Font for window titles. Will also be used by the bar unless a different font
# is used in the bar {} block below.

# Start Minecraft & Netflix
bindsym $mod+z exec --no-startup-id sol
bindsym $mod+m exec --no-startup-id minecraft-launcher
#bindsym $mod+n exec --no-startup-id ice-firefox https://www.netflix.com
#bindsym $mod+Shift+F1 exec --no-startup-id ice-firefox https://www.bbc.co.uk/iplayer
#bindsym $mod+Shift+F3 exec --no-startup-id ice-firefox https://www.itv.com
#bindsym $mod+Shift+F4 exec --no-startup-id ice-firefox https://www.channel4.com
#bindsym $mod+Shift+F5 exec --no-startup-id ice-firefox https://www.channel5.com
#bindsym $mod+Shift+d exec --no-startup-id ice-firefox https://www.disneyplus.com
bindsym $mod+n exec --no-startup-id firefox --kiosk https://www.netflix.com
bindsym $mod+Shift+F1 exec --no-startup-id firefox --kiosk https://www.bbc.co.uk/iplayer
bindsym $mod+Shift+F3 exec --no-startup-id firefox --kiosk https://www.itv.com
bindsym $mod+Shift+F4 exec --no-startup-id firefox --kiosk https://www.channel4.com
bindsym $mod+Shift+F5 exec --no-startup-id firefox --kiosk https://www.channel5.com
bindsym $mod+Shift+d exec --no-startup-id firefox --kiosk https://www.disneyplus.com
# Use Mouse+$mod to drag floating windows to their wanted position
floating_modifier $mod

# start a terminal
bindsym $mod+Return exec --no-startup-id $term 

# kill focused window
bindsym $mod+Shift+q kill

# start dmenu (a program launcher)
bindsym $mod+d exec --no-startup-id wofi --show drun 

# change focus
bindsym $mod+j focus left
bindsym $mod+k focus down
bindsym $mod+l focus up
bindsym $mod+semicolon focus right

# alternatively, you can use the cursor keys:
bindsym $mod+Left focus left
bindsym $mod+Down focus down
bindsym $mod+Up focus up
bindsym $mod+Right focus right

# make some things use floating layout by default
 for_window [window_role="pop-up"]       floating enable
 for_window [window_role="bubble"]       floating enable
 for_window [window_role="task_dialog"]  floating enable
 for_window [window_role="Preferences"]  floating enable
 for_window [window_type="dialog"]       floating enable
 for_window [window_type="menu"]         floating enable


# move focused window
bindsym $mod+Shift+j move left
bindsym $mod+Shift+k move down
bindsym $mod+Shift+l move up
bindsym $mod+Shift+semicolon move right

# alternatively, you can use the cursor keys:
bindsym $mod+Shift+Left move left
bindsym $mod+Shift+Down move down
bindsym $mod+Shift+Up move up
bindsym $mod+Shift+Right move right

# split in horizontal orientation
bindsym $mod+h split h

# split in vertical orientation
bindsym $mod+v split v

# enter fullscreen mode for the focused container
bindsym $mod+f fullscreen toggle

# change container layout (stacked, tabbed, toggle split)
bindsym $mod+s layout stacking
bindsym $mod+w layout tabbed
bindsym $mod+e layout toggle split

# toggle tiling / floating
bindsym $mod+Shift+space floating toggle

# change focus between tiling / floating windows
bindsym $mod+space focus mode_toggle

# focus the parent container
bindsym $mod+a focus parent

# focus the child container
#bindsym $mod+d focus child

# switch to workspace
bindsym $mod+1 workspace 1
bindsym $mod+2 workspace 2
bindsym $mod+3 workspace 3
bindsym $mod+4 workspace 4
bindsym $mod+5 workspace 5
bindsym $mod+6 workspace 6
bindsym $mod+7 workspace 7
bindsym $mod+8 workspace 8
bindsym $mod+9 workspace 9
bindsym $mod+0 workspace 10

# move focused container to workspace
bindsym $mod+Shift+1 move container to workspace 1
bindsym $mod+Shift+2 move container to workspace 2
bindsym $mod+Shift+3 move container to workspace 3
bindsym $mod+Shift+4 move container to workspace 4
bindsym $mod+Shift+5 move container to workspace 5
bindsym $mod+Shift+6 move container to workspace 6
bindsym $mod+Shift+7 move container to workspace 7
bindsym $mod+Shift+8 move container to workspace 8
bindsym $mod+Shift+9 move container to workspace 9
bindsym $mod+Shift+0 move container to workspace 10

# reload the configuration file
bindsym $mod+Shift+c reload
# restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
bindsym $mod+Shift+r restart
# exit i3 (logs you out of your X session)
bindsym $mod+Shift+e exec "i3-nagbar -t warning -m 'You pressed the exit shortcut. Do you really want to exit i3? This will end your X session.' -b 'Yes, exit i3' 'i3-msg exit'"

# resize window (you can also use the mouse for that)
mode "resize" {
        # These bindings trigger as soon as you enter the resize mode

        # Pressing left will shrink the window’s width.
        # Pressing right will grow the window’s width.
        # Pressing up will shrink the window’s height.
        # Pressing down will grow the window’s height.
        bindsym j resize shrink width 10 px or 10 ppt
        bindsym k resize grow height 10 px or 10 ppt
        bindsym l resize shrink height 10 px or 10 ppt
        bindsym semicolon resize grow width 10 px or 10 ppt

        # same bindings, but for the arrow keys
        bindsym Left resize shrink width 10 px or 10 ppt
        bindsym Down resize grow height 10 px or 10 ppt
        bindsym Up resize shrink height 10 px or 10 ppt
        bindsym Right resize grow width 10 px or 10 ppt

        # back to normal: Enter or Escape
        bindsym Return mode "default"
        bindsym Escape mode "default"
}

exec_always --no-startup-id nitrogen --restore 
bindsym $mod+Shift+BackSpace exec --no-startup-id reboot
bindsym $mod+Shift+X exec --no-startup-id shutdown -h now
bindsym $mod+r mode "resize"

## Possible StatusBars

## Firstly i3status. (uncomment this)

# Start i3bar to display a workspace bar (plus the system information i3status
# finds out, if available)
#bar {
#       status_command i3status
#}

## Second Piping conky through the statusbar (uncomment this)

# bar {
#         status_command $HOME/conky-i3bar
#   	position bottom
#  }

## Lastly to execute polybar. (uncomment this) 

#    exec --no-startup-id $HOME/.config/polybar/launch.sh

#Launch Polybar where appropriate:
  #exec_always --no-startup-id ~/.config/polybar/launch.sh
bar {
	swaybar_command waybar
				}
#-- terminal --#
exec_always --no-startup-id alacritty --name=alacritty_scratch
#for_window[instance="alacritty_scratch"] move scratchpad

bindsym Mod1+Return \
    [instance="python_scratch|r_scratch|tasks_scratch|julia_scratch"] move scratchpad; \
    [instance="alacritty_scratch"] scratchpad show, resize set $scratch_width $scratch_height; \
    move position $scratch_x1 $scratch_y1;

#-- r console --#
# exec_always --no-startup-id alacritty -e "radian --no-environ" --name=r_scratch
exec_always --no-startup-id alacritty -e "radian" --name=r_scratch
#for_window[instance="r_scratch"] move scratchpad

bindsym Mod1+space \
    [instance="python_scratch|alacritty_scratch|tasks_scratch|julia_scratch"] move scratchpad; \
    [instance="r_scratch"] scratchpad show, resize set $scratch_width $scratch_height; \
    move position $scratch_x1 $scratch_y1

#-- python console --#
exec_always --no-startup-id alacritty -e ipython --name=python_scratch
#for_window[instance="python_scratch"] move scratchpad

bindsym Mod1+p \
    [instance="r_scratch|alacritty_scratch|tasks_scratch|julia_scratch"] move scratchpad; \
    [instance="python_scratch"] scratchpad show, resize set $scratch_width $scratch_height; \
    move position $scratch_x1 $scratch_y1

#-- julia console --#
exec_always --no-startup-id alacritty -e julia --name=julia_scratch
#for_window[instance="julia_scratch"] move scratchpad

