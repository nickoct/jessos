#!/bin/bash
get_profile () {
	sudo [ -d "/home/$un/JessOS/profiles" ] && sudo mv /home/$un/JessOS/profiles /usr/share/archiso/configs/
	sudo cp -r /usr/share/archiso/configs/profiles/$de/* /home/$un/JessOS/
	build_process
	exit
		}
profile_menu () {
	clear
	while true
	do
		echo " 1. cinnamon "
		echo " 2. gnome "
		echo " 3. i3 "
		echo " 4. lxde "
		echo " 5. mate "
		echo " 6. openbox "
		echo " 7. xfce4 "
		echo " 8. awesome "
		echo " 9. baseline "
		echo " 0. sway "
		echo " q. Quit program "
		echo -e "\n"
		echo -e "Choose your profile "
		read ans
		case "$ans" in
			1) de="cinnamon"
				get_profile ;;
			2) de="gnome"
				get_profile ;;
			3) de="i3"
				get_profile ;;
			4) de="lxde"
				get_profile ;;
			5) de="mate"
				get_profile ;;
			6) de="openbox"
				get_profile ;;
			7) de="xfce4"
				get_profile ;;
			8) de="awesome"
				get_profile ;;
			9) de="baseline"
				get_profile ;;	
			0) de="sway"
				get_profile ;;	
			q) exit ;;	
		esac
	done
}
build_process ()
{
sudo mkarchiso -v -w /home/$un/JessOS/work -o /home/$un/JessOS/out /home/$un/JessOS
sudo [ ! -d /home/$un/ISOs ] && mkdir /home/$un/ISOs
sudo cp -r /home/$un/JessOS/out/*.iso /home/$un/ISOs/JessOS-$de.iso
sudo chown $un:wheel -R /home/$un/ISOs/
sudo rm -r /home/$un/JessOS/*
}

un=$SUDO_USER
sudo [ ! -d /home/$un/ISOs ] && sudo mkdir /home/$un/ISOs
sudo [ -f /home/$un/JessOS/AURpackages.sh ] && sudo chown -r $un:wheel /home/$un/JessOS/AURpackages.sh sudo mv /home/$un/JessOS/AURpackages.sh /home/$un/
sudo [ -f /home/$un/JessOS/build.sh ] && sudo mv /home/$un/JessOS/build.sh /usr/local/bin/
profile_menu
