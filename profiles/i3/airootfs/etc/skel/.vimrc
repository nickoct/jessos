syntax enable  		" enable syntax processing
set showcmd             " show command in bottom bar
set showcmd             " show command in bottom bar
set number              " show line numbers
set showcmd             " show command in bottom bar
set cursorline          " highlight current line
nnoremap gV `[v`]	" highlight last inserted text
vmap <F6> :!xclip -f -sel clip<CR>
map <F7> :-1r !xclip -o -sel clip<CR>
