#!/bin/bash

sudo mkarchiso -v -w /home/peter/archlive/work -o /home/peter/archlive/out /home/peter/archlive

sudo cp -r /home/peter/archlive/out/*.iso /home/peter/ISOs
sudo chown peter:wheel -R /home/peter/ISOs/

sudo rm -r /home/peter/archlive/work /home/peter/archlive/out
